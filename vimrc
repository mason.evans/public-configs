let environmenttype=$ENVIRONMENT_TYPE

syntax enable


" Enable file type detection
filetype plugin indent on

" show existing tab with 2 spaces width
set tabstop=4
" when indenting with '>', use 2 spaces width
set shiftwidth=4

au BufRead,BufNewFile *.fish	setfiletype sh

" set up file-type specific tab settings
au FileType html setlocal shiftwidth=2 tabstop=2
au FileType yaml setlocal expandtab shiftwidth=2 tabstop=2
au FileType python setlocal expandtab shiftwidth=4 tabstop=4
au FileType nix setlocal expandtab shiftwidth=2 tabstop=2
au FileType haskell setlocal expandtab shiftwidth=2 tabstop=2

" Turn on indent for python folding and map za to space
" au FileType python setlocal foldmethod=indent
" nnoremap <space> za

" Turn on line numbering and relative line numbering
set number
set relativenumber

set hidden
set showcmd

set cursorline " highlight cursor line

set wildmenu " show file list when cycling with :e autocomplete

set showmatch " show matching paren

set hlsearch " higlight all search matches
set ignorecase
set smartcase

set confirm

set cmdheight=2

set backspace=indent,start,eol

" press backslash-space to unhighlight the last seaerch
nnoremap <leader><space> :nohlsearch<CR>

" This makes gj/gk move by virtual lines when used without a count, and by
" physical lines when used with a count. This is perfect in tandem with
" relative numbers.
noremap <silent> <expr> j (v:count == 0 ? 'gj' : 'j')
noremap <silent> <expr> k (v:count == 0 ? 'gk' : 'k')

" Control-n to toggle line numbers
function! RelNumberToggle()
	if(&relativenumber == 1)
		set norelativenumber
	else
		set relativenumber
	endif
endfunc

function! AllNumberToggle()
	if(&relativenumber == 1 && &number == 1)
		set norelativenumber
		set nonumber
	else
		set number
		set relativenumber
	endif
endfunc

nnoremap <C-n> :call RelNumberToggle()<cr>

nnoremap <C-N> :call AllNumberToggle()<cr>

" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %

" Make :qq close all,  :ww save all, :xx exit all
cmap qq qa
cmap ww wa
cmap xx xa

" Control-t to create new vertical split
nnoremap <C-t> :vs<cr>


if environmenttype == 'local'
	colorscheme badwolf        " black for local

	" git gutter settings
	set laststatus=2
	set updatetime=250
	if exists('&signcolumn')
		set signcolumn=yes
	endif

	" Turn on indent guides for yaml
	au FileType yaml IndentGuidesEnable
	au FileType python IndentGuidesEnable
	au FileType nix IndentGuidesEnable

	" ALE Linters
	let g:ale_linters = {
				\ 'python': ['flake8', 'pylint'],
	\}

	let g:ale_fixers = {
				\ '*': ['remove_trailing_lines', 'trim_whitespace'],
				\ 'python': ['black'],
    \}

	" show file name in iterm title bar
	set t_ts=]1;
	set t_fs=
	set title titlestring=
	set titleold=

	" Set up fzf key bindings
	noremap <C-p> :Files<cr>
	noremap <C-o> :Buffers<cr>
endif
